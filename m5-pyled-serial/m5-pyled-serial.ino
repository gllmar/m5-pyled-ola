/*

*/

#include "M5Atom.h"
int RGB_MAP[] = {1, 0, 2}; // GRB

int baud_selected = 15;

int  baudRates[] = {
110,    // 0
300,    // 1
600,    // 2
1200,   // 3
1800,   // 4
2400,   // 5
4800,   // 6
9600,   // 7
14400,  // 8 
19200,  // 9
38400,  // 10
56000,  // 11
57600,  // 12 
115200, // 13
230400, // 14
250000, // 15
460800, // 16
500000, // 17
750000, // 18
921600, // 19
1000000, // 20
1500000, // 21
2000000, // 22
};


#include <Adafruit_NeoPixel.h>  
const int myPixelCount = 170;
const int myPixelPin = 26; //26 -> m5stack compatible pin for HEX_SK6812
const int m5PixelPin = 27; //26 -> m5stack pin for integrated led
Adafruit_NeoPixel myPixelStrip(myPixelCount, myPixelPin , NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel m5PixelStrip(1, m5PixelPin , NEO_GRB + NEO_KHZ800);


#include <MicroOscSlip.h>
MicroOscSlip<1800> myMicroOsc(&Serial);
MicroOscSlip<30> myMicroOscBtn(&Serial);

void test_pixels()
{
  // STARTUP ANIMATION
  // LIGHT ALL PIXELS IN WHITE THEN TURN THEM ALL OFF
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(10, 0, 0));  //  less bright
  m5PixelStrip.show();


  for ( int i=0; i < myPixelCount; i++ ) {
     //myPixelStrip.setPixelColor(i, myPixelStrip.Color(255, 255 , 255));  
     myPixelStrip.setPixelColor(i, myPixelStrip.Color(10, 10, 10));  //  less bright
     myPixelStrip.show();
     delay(10);                                       
  }
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(0, 10, 0));  //  less bright
  m5PixelStrip.show(); 
  delay(1000);
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(0, 0, 10));  //  less bright

  myPixelStrip.clear();
  myPixelStrip.show();
  m5PixelStrip.clear();
  m5PixelStrip.show();
}




void setup() {
  M5.begin(false, false, false);

  // initialize serial:
  Serial.begin(baudRates[baud_selected]);
  delay(100);
  Serial.println("");
  Serial.print("Baudrate is: ");
  Serial.println(baudRates[baud_selected]);
  // reserve 1024 bytes for the inputString:
    // INITIALIZE PIXEL STRIP
  myPixelStrip.begin();
  m5PixelStrip.begin(); 
  test_pixels();
}


// FUNCTION THAT IS CALLED FOR EVERY RECEIVED OSC MESSAGE
void myOnReceiveMessage( MicroOscMessage& receivedOscMessage ) {
  


  // IF THE ADDRESS IS /rgb
  if ( receivedOscMessage.fullMatch("/rgb") ) {   
      m5PixelStrip.setPixelColor(0, myPixelStrip.Color(10, 0, 10));  //  less bright
      m5PixelStrip.show();
                 
    // CREATE A VARIABLE TO STORE THE POINTER TO THE DATA
    const uint8_t* blobData;                                    
    // GET THE DATA SIZE AND THE POINTER TO THE DATA
    int blobSize = receivedOscMessage.nextAsBlob(&blobData);
            //Serial.println(blobSize);

//    myMicroOsc.sendFloat( "/test/blobsize", blobSize);   
    // IF DATA SIZE IS LARGER THAN 0 AND IS A MULTIPLE OF 3 AS REQUIRED BY RGB PIXELS
    if ( blobSize > 0 && blobSize % 3 == 0 ) {                  
      // DIVIDE THE DATA BY 3 TO GET THE NUMBER OF PIXELS
      int blobPixelCount = blobSize / 3;                        
      // ITERATE THROUGH EACH PIXEL IN THE BLOB 
      for ( int i = 0 ; i < blobPixelCount ; i++ ) {            
        // EACH PIXEL HAS 3 BYTES, SO WE GO THROUGH THE DATA, 3 AT A TIME
        int blobIndex = i * 3; 
        uint8_t red =  myPixelStrip.gamma8(blobData[blobIndex]);
        uint8_t green =  myPixelStrip.gamma8(blobData[blobIndex+1]); 
        uint8_t blue =  myPixelStrip.gamma8(blobData[blobIndex+2]);                            
        myPixelStrip.setPixelColor(i, myPixelStrip.Color(red , green , blue));
      }
      uint8_t red =  myPixelStrip.gamma8(blobData[0]);
      uint8_t green =  myPixelStrip.gamma8(blobData[0+1]); 
      uint8_t blue =  myPixelStrip.gamma8(blobData[0+2]);          
      m5PixelStrip.setPixelColor(0, m5PixelStrip.Color(red , green , blue));
      m5PixelStrip.show();
      myPixelStrip.show();
    }
  }
}


void loop() 
{
 myMicroOsc.receiveMessages( myOnReceiveMessage );

  M5.update();  
  if (M5.Btn.wasPressed()) { 
    myMicroOscBtn.sendInt( "/btn" ,1 );
    test_pixels();
    }
  if (M5.Btn.wasReleased()) { myMicroOscBtn.sendInt( "/btn" ,0 );}
}

