import serial
import random
import argparse
import time
import struct

# SLIP special character definitions
SLIP_END     = 0xC0
SLIP_ESC     = 0xDB
SLIP_ESC_END = 0xDC
SLIP_ESC_ESC = 0xDD

def osc_encode(address, types, arguments):
    """
    Encode given address, types, and arguments to OSC format
    """
    msg = []

    # Add address (string) to the message
    msg.extend(list(address.encode()))
    msg.append(0)  # Null termination for string
    # Padding for 4-byte alignment
    while len(msg) % 4:
        msg.append(0)

    # Add types (string) to the message
    msg.extend(list(types.encode()))
    msg.append(0)  # Null termination for string
    # Padding for 4-byte alignment
    while len(msg) % 4:
        msg.append(0)

    # Add size of blob as int32
    blob_size = len(arguments)
    msg.extend(blob_size.to_bytes(4, byteorder='big'))

    # Add arguments (bytearray) to the message
    msg.extend(arguments)

    # Padding for 4-byte alignment
    while len(msg) % 4:
        msg.append(0)

    return bytearray(msg)

def slip_encode(msg):
    """
    Encode given list of integers to SLIP format
    """
    encoded_msg = []

    # Append the END character to the output message to flush out any data
    # that may have been cut off from the previous message
    encoded_msg.append(SLIP_END)

    for b in msg:
        if b == SLIP_END:
            encoded_msg.append(SLIP_ESC)
            encoded_msg.append(SLIP_ESC_END)
        elif b == SLIP_ESC:
            encoded_msg.append(SLIP_ESC)
            encoded_msg.append(SLIP_ESC_ESC)
        else:
            encoded_msg.append(b)

    # Append the END character to the output message
    encoded_msg.append(SLIP_END)

    return bytearray(encoded_msg)

def slip_decode(msg):
    """
    Decode a SLIP message
    """
    decoded_msg = []
    i = 0
    while i < len(msg):
        if msg[i] == SLIP_ESC:
            i += 1
            if i < len(msg):
                if msg[i] == SLIP_ESC_END:
                    decoded_msg.append(SLIP_END)
                elif msg[i] == SLIP_ESC_ESC:
                    decoded_msg.append(SLIP_ESC)
        else:
            decoded_msg.append(msg[i])
        i += 1
    return decoded_msg

def osc_decode(osc_msg):
    """
    Decode an OSC message to a human-readable format
    """
    osc_msg = bytes(osc_msg)

    # Remove any leading SLIP_END characters
    while osc_msg and osc_msg[0] == SLIP_END:
        osc_msg = osc_msg[1:]

    if not osc_msg:
        return "<empty message>"

    # First null-terminated string represents the address
    address_end = osc_msg.index(0)  # Null-terminated string
    address = osc_msg[:address_end].decode()

    # The next null-terminated string after the address represents the types
    types_start = ((address_end // 4) + 1) * 4  # Next 4-byte boundary
    types_end = osc_msg.index(0, types_start)  # Null-terminated string
    types = osc_msg[types_start:types_end].decode()

    # The rest of the message are the arguments
    arguments_start = ((types_end // 4) + 1) * 4  # Next 4-byte boundary
    arguments = osc_msg[arguments_start:]

    result = [address]

    # Unpack arguments according to the types
    for t in types[1:]:  # Skip the first character, which is a comma
        if t == 'f':
            # Unpack as a float
            arg, arguments = arguments[:4], arguments[4:]
            arg_value = struct.unpack('>f', arg)[0]
        elif t == 'i':
            # Unpack as an int
            arg, arguments = arguments[:4], arguments[4:]
            arg_value = struct.unpack('>i', arg)[0]
        elif t == 's':
            # Unpack as a string
            end = arguments.index(0)  # Null-terminated string
            arg, arguments = arguments[:end], arguments[end+1:]
            arg_value = arg.decode()
        elif t == 'b':
            # Unpack as a blob
            size, arguments = arguments[:4], arguments[4:]
            size_value = struct.unpack('>i', size)[0]
            arg, arguments = arguments[:size_value], arguments[size_value:]
            arg_value = arg  # leave it as bytes
        else:
            raise ValueError(f'Unsupported type: {t}')
        result.append(arg_value)

    return ' '.join(str(r) for r in result)



def transmit_message(port, baudrate, msg):
    """
    Transmit the given message over serial connection
    """
    with serial.Serial(port, baudrate, timeout=1) as ser:
        ser.write(msg)
        time.sleep(1)  # Give the Arduino time to process and respond

        # Now read the response
        while ser.in_waiting > 0:
            response = ser.read(ser.in_waiting)
            decoded_response = slip_decode(response)
            print(f"Received from Arduino: {osc_decode(decoded_response)}")


def print_encoded_msg(msg):
    """
    Print the encoded message as a space-separated string of integers
    """
    print(' '.join(map(str, msg)))

def main(port, baudrate):
    # Start with the address "/rgb" and type "b"
    address = "/rgb"
    types = ",b"

    # Generate a list of 510 integers
    arguments = [100 for _ in range(510)]

    # Encode the message in OSC format
    osc_msg = osc_encode(address, types, arguments)

    # Encode the OSC message in SLIP format
    slip_msg = slip_encode(osc_msg)

    # Print the encoded message
    print_encoded_msg(slip_msg)

    # Transmit the message
    transmit_message(port, baudrate, slip_msg)
    time.sleep(1)

if __name__ == '__main__':
    # Setup the command line arguments
    parser = argparse.ArgumentParser(description='Transmit SLIP encoded messages over serial connection.')
    parser.add_argument('port', help='The serial port to transmit over.')
    parser.add_argument('baudrate', type=int, help='The baudrate to use for the serial connection.')
    args = parser.parse_args()

    # Call the main function with the command line arguments
    main(args.port, args.baudrate)
